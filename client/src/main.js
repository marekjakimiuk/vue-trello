// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from '@/store/store'
import VModal from 'vue-js-modal'
import { sync } from 'vuex-router-sync'

Vue.config.productionTip = true

Vue.use(VModal, { dynamic: true })

sync(store, router)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  VModal,
  template: '<App/>',
  components: { App }
})
