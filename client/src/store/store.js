import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

// import createPersistedState from 'vuex-persistedstate'

// Modules
// import task from './modules/task TODO' CLEAN_MODULES

Vue.use(Vuex)

export default new Vuex.Store({
  // modules: { TODO CLEAN_MODULES
  //   task
  // },
  // plugins: [
  //   createPersistedState()
  // ],
  state: {
    // user: localUser,
    token: localStorage.getItem('token') || null,
    isLoggedIn: !!(localStorage.getItem('token')),
    cards: Array,
    lists: Array,
    labels: Array,
    board: Object
  },
  getters: {
    getTaskCards (state) {
      return state.cards
    },
    getTaskLists (state) {
      return state.lists
    },
    getLabels (state) {
      return state.labels
    },
    getBoard (state) {
      return state
    },
    getBoardName (state) {
      return state.board.name
    },
    getUserToken (state) {
      return state.token
    },
    getUser (state) {
      return state.user
    },
    isLoggedIn (state) {
      return state.token
    }
  },
  mutations: {
    initTaskCard (state, dataMock) {
      state.cards = dataMock.cards
      state.lists = dataMock.lists
      state.labels = dataMock.labels
      state.board = dataMock
    },
    updateList (state, value) {
      state.cards = value
    },
    addNewTaskCard (state, { taskName, taskId, taskListId, taskTagsId }) {
      const task = {
        name: taskName,
        id: taskId,
        idList: taskListId,
        taskTagsId: taskTagsId
      }
      state.cards.push(task)
    },
    deleteTaskCard (state, idCard) {
      state.cards = state.cards.filter((card) => (card.id !== idCard))
    },
    login (state, { username, password }) {
      axios.post({
        method: 'POST',
        url: 'http://localhost:8081/login',
        data: {
          username: this.username,
          password: this.password
        }
      })
    },
    //
    // POWYZEJ DO SPRAWDZENIA
    //
    setToken (state, token) {
      state.token = token
      state.isLoggedIn = !!(token)
    }
  },
  actions: {
    init ({ commit }) {
      fetch('/static/mock-board.json')
        .then((res) => res.json())
        .then((dataMock) => {
          commit('initTaskCard', dataMock)
        })
    },
    //
    // POWYZEJ DO SPRAWDZENIA
    //
    setToken ({commit}, token) {
      localStorage.setItem('token', token)
      commit('setToken', token)
    }
    // setUser ({commit}, user) {
    //   console.log(user)
    //   localStorage.setItem('user', JSON.stringify(user))
    //   commit('setUser', user)
    // }
  },
  strict: true
})
