import axios from 'axios'
import store from '@/store/store'

export default () => {
  console.log('store.getters.getUserToken', store.getters.getUserToken)
  return axios.create({
    baseURL: `http://localhost:8081/`,
    headers: {
      Authorization: `Bearer ${store.getters.getUserToken}`
    }
  })
}
