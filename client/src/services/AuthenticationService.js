import Api from '@/services/Api'

export default {
  register (credentials) {
    return Api().post('register', credentials)
  },
  login (credentials) {
    return Api().post('login', credentials)
  },
  checktoken (credentials) {
    console.log('check')
    return Api().post('checktoken', credentials)
  }
}
