import Vue from 'vue'

import Main from '@/components/Main'
import Auth from '@/components/Auth.vue'
import Check from '@/components/Check.vue'
import Contact from '@/components/Contact.vue'
import Page404 from '@/components/Page404.vue'
import Styleguide from '@/components/Styleguide.vue'
import TaskListContainer from '@/components/TaskListContainer.vue'

// Middleware
import VueRouter from 'vue-router'

// import VueRouterMiddleware, { middleware, createMiddleware } from 'vue-router-middleware'

const routes = [
  {
    path: '/',
    name: 'Main',
    component: Main
  },
  {
    path: '/auth',
    name: 'Auth',
    component: Auth
  },
  {
    path: '*',
    name: 'Page404',
    component: Page404
  },
  {
    path: '/styleguide',
    name: 'styleguide',
    component: Styleguide
  },
  {
    path: '/board',
    name: 'board',
    component: TaskListContainer
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact
  },
  {
    path: '/check',
    name: 'Check',
    component: Check
  }
]

const router = new VueRouter({ routes, mode: 'history' })

Vue.use(VueRouter)
// Vue.use(VueRouterMiddleware, { router })

// eslint-disable-next-line
export default router
