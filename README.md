# Informations

> Trello app based on Vue.js

# Requires
``` bash
# node.js version
v10.6.0

# yarn
brew install yarn --without-node
```


# Installation
``` bash
# install dependencies
yarn
```

# Usage
``` bash
# serve with hot reload at localhost:8080
yarn dev

# build for production with minification
yarn build

# build for production and view the bundle analyzer report
yarn build --report
```

# SPRINTS:

### 1 Sprint (14-20 MAY)
- [x] Initial commit
- [x] Store based on vuex

### 2 Sprint (21-27 MAY)
- [In Progress] Draggable.js
- [x] Sortable.js
- [x] Modal.js
- [x] Readme remake

### 3 Sprint (21-27 MAY)
- [x] Nothing

### 4 Sprint (27 MAY - 22 July )
- [ ] Nothing

### 5 Sprint (23-28 July)
- [ ] Create Server-side
- [ ] Authorization
- [x] Registration
- [x] JWT (put creditials into localstorage)
- [x] Add JWT (Json web token)
- [x] Mysql

### 6 Sprint (28-30 July - 1-31 August)
- [ ] Done all features from backwards also code clean
- [ ] Include PM2 for better manage app

# Planning
- [ ] Make sure for better security and validation
- [ ] vue-js-popover
- [ ] refactor names
- [ ] clean code
- [ ] https://github.com/vue-a11y/vue-announcer
