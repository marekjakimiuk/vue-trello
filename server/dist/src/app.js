'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _utilities = require('./utilities/');

var _connectiondb = require('./config/connectiondb.js');

var _user = require('./models/user.js');

var _token = require('./models/token.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import { IfObservable } from 'rxjs/observable/IfObservable';
// import { taskModel } from './models/task.js'
// import { boardModel } from './models/board.js'

var app = (0, _express2.default)();
app.use((0, _morgan2.default)('combine'));
app.use(_bodyParser2.default.json());
app.use((0, _cors2.default)());
// app.use(bodyParser.urlencoded({extended: true}))

var user = _connectiondb.connectionDB.define('user', _user.userModel);
// const board = connectionDB.define('board', boardModel)
// const task = connectionDB.define('task', taskModel)

app.post('/board', _utilities.verifyToken, function (req, res) {
  _jsonwebtoken2.default.verify(req.token, 'secretkey', function (err, authData) {
    if (err) {
      res.json({
        error: 'Dont have access'
      });
    } else {
      res.json({
        message: 'hidden message',
        authData: authData
      });
    }
  });
});

app.post('/login', function (req, res) {
  if (req.body.username === 'maro' && req.body.password === 'maro') {
    _jsonwebtoken2.default.sign({ user: user }, 'secretkey', { expiresIn: '7d' }, function (error, token) {
      if (error) {
        res.json({
          message: 'Nie mozesz sie zalogowac'
        });
      } else {
        res.json({
          token: token,
          message: 'Zalogowales sie'
        });
      }
    });
  } else {
    res.json({
      token: null
    });
  }
});

app.post('/register', function (req, res) {
  var reqBack = req.body;
  _connectiondb.connectionDB.sync({
    force: true,
    logging: console.log
  }).then(function () {
    return user.create({
      email: reqBack.email,
      username: reqBack.username,
      password: reqBack.password,
      lastname: reqBack.lastname,
      firstname: reqBack.firstname
    });
  }).catch(function (error) {
    console.log('poczatek', error.errors[0].message, 'koniec');
  });
});

app.get('/getusers', function (req, res, error) {
  res.json({ message: 'GOOD' }).then(function () {
    console.log('costam');
  }).catch(error, function () {
    console.log('failure');
  });
});

app.listen(process.env.PORT || 8081);
//# sourceMappingURL=app.js.map