/* eslint-disable */
const passport = require('passport')

module.exports = (req, res, next) => {
	return passport.authenticate('jwt', { session: false }, (err, user) => {
			
		if(err || !user){
				
			res.json({ error: 'Unauthorized' }, 403);

		}else{

			req.user = user;
			next();

		}

	})(req, res, next);
}

// module.exports = (is_required) => {
	
// 	return (req, res, next) => {

// 		return passport.authenticate('jwt', { session: false }, (err, user) => {
		
// 			console.log(user)
// 			if (!user) {
// 				console.log('error', {err})
// 				res.status(403).send({
// 			error: 'youds do not have access to this resource'
// 			})
// 		} else {
// 			console.log('req.user', req.user)
// 			req.user = user
// 			next()
// 		}
// 		})(req, res, next);
	
// 	}

// }