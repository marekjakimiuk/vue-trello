const { User } = require('./models')

const passport = require('passport')
const JwtStrategy = require('passport-jwt').Strategy
const JwtExtract = require('passport-jwt').ExtractJwt

const config = require('./config/config')

console.log('wchodzi w passport', JwtExtract.fromAuthHeaderAsBearerToken())

// console.log('config.authentication.jwtSecret', config.authentication.jwtSecret)
passport.use(
  new JwtStrategy({
    jwtFromRequest: JwtExtract.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.authentication.jwtSecret
  }, async (data, done) => {
    console.log('data', data)
    console.log('jwtPayload', JwtExtract.fromAuthHeaderAsBearerToken())
    // console.log('done', done)
    const user = await User.findOne({
      where: {
        email: data.email
      }
    })
    if (!user) {
      console.log('1q', data)
      return done(null, false)
    }
    console.log('2q')
    return done(null, user)
  })
)
