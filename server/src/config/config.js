// const path = require('path')

module.exports = {
  port: process.env.PORT || 8081,
  db: {
    database: process.env.DB_NAME || 'vuetrello',
    user: process.env.DB_USER || 'root',
    password: process.env.DB_PASS || 'koko12',
    options: {
      operatorsAliases: false, // TODO
      dialect: process.env.DIALECT || 'mysql',
      host: process.env.HOST || 'localhost' // ,
      // storage: path.resolve(__dirname, '../../tabtracker.sqlite')
    }
  },
  authentication: {
    jwtSecret: process.env.JWT_SECRET || 'secret'
  }
}
