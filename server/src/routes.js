const AuthenticationController = require('./controllers/AuthenticationController')
const AuthenticationControllerPolicy = require('./controllers/AuthenticationControllerPolicy')

const isAuthenticated = require('./isAuthenticated')

module.exports = (app) => {
  app.post('/login',
    AuthenticationController.login)

  app.post('/register', AuthenticationController.register)

  app.post('/checktoken',
    isAuthenticated,
    AuthenticationController.check)
}
