/* eslint-disable */
const { User } = require('../models')
const jwt = require('jsonwebtoken')

function jwtSignUser (user) {
  // console.log('user.dataValues', user.dataValues)
  return jwt.sign(user.dataValues, 'secret', {
    expiresIn: 86400
  })
}

// TODO POSPRzATAC

module.exports = {
  async register (req, res) {
    try {
      const user = await User.create(req.body)
      const userJson = user.toJSON()
      res.send({
        user: userJson,
        token: jwtSignUser(userJson)
      })
    } catch (err) {
      res.json({
        error: 'This username account is already in use.'
      })
    }
  },
  login (req, res) {
    const { email, password } = req.body
    User.findOne({
      where: {
        email: email
      }
    })
      .then(user => {
		  
        // console.log('user.dataValues.password', user.dataValues.password)
        if (user.dataValues.password === password) { // TODO przywrocic haszowanie
          // console.log('user then', user)
          res.json({
            user: user,
            token: jwtSignUser(user)
          })
        }
      })
      .catch((err) => {
        console.log('5')
        res.status(500).send({
          err: err,
          error: 'An error has occured trying to log in'
        })
        console.log('cos 5')
      })
  },
  check (req, res) {
	console.log('checked')
	console.dir(res);
    res.json({
      error: 'chyba dziala.'
    })
  }
}
