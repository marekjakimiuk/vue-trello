import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import morgan from 'morgan'
const {sequelize} = require('./models')
const config = require('./config/config')
// import jwt from 'jsonwebtoken'

var fs = require('fs')
var path = require('path')
// import { User } from './models/User'

// import { tokenModel } from './models/token'
// import { taskModel } from './models/task.js'
// import { boardModel } from './models/board.js'
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })

const app = express()
app.use(morgan('combined', { stream: accessLogStream }))
app.use(bodyParser.json())
app.use(cors())
// app.use(bodyParser.urlencoded({extended: true}))

// const board = DB.define('board', boardModel)
// const task = DB.define('task', taskModel)

require('./passport')
require('./routes')(app)

sequelize.sync({force: false})
  .then(() => {
    app.listen(config.port)
    // console.log(`Server started on port ${config.port}`)
  })
